package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.endpoint.EndpointLocator;
import ru.ekfedorov.tm.bootstrap.Bootstrap;
import ru.ekfedorov.tm.marker.IntegrationCategory;

import java.util.List;


public class TaskEndpointTest {

    final EndpointLocator endpointLocator = new Bootstrap();

    private Session session;

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        session = endpointLocator.getSessionEndpoint().openSession("test", "test");
        sessionAdmin = endpointLocator.getSessionEndpoint().openSession("admin", "admin");
        endpointLocator.getTaskEndpoint().clearBySessionTask(session);
        endpointLocator.getTaskEndpoint().clearBySessionTask(sessionAdmin);
        endpointLocator.getProjectEndpoint().clearBySessionProject(session);
        endpointLocator.getProjectEndpoint().clearBySessionProject(sessionAdmin);
    }

    @After
    @SneakyThrows
    public void after() {
        endpointLocator.getTaskEndpoint().clearBySessionTask(session);
        endpointLocator.getTaskEndpoint().clearBySessionTask(sessionAdmin);
        endpointLocator.getSessionEndpoint().closeSession(session);
        endpointLocator.getSessionEndpoint().closeSession(sessionAdmin);

    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void addTaskTest() {
        final String taskName = "nameTest";
        final String taskDescription = "nameTest";
        endpointLocator.getTaskEndpoint().addTask(session, taskName, taskDescription);
        final Task task = endpointLocator.getTaskEndpoint().findTaskOneByName(session, taskName);
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getName());
        Assert.assertEquals(taskDescription, task.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void bindTaskByProjectTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        final Task taskBind = taskEndpoint.bindTaskByProject(session, project.getId(), task.getId());
        Assert.assertNotNull(taskBind);
        Assert.assertEquals(project.getId(), taskBind.getProjectId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeTaskStatusByIdTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.changeTaskStatusById(session, task.getId(), Status.COMPLETE);
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeTaskStatusByIndexTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        final List<Task> tasks = taskEndpoint.findAllTask(session);

        int pos = 0;
        for(Task t : tasks) {
            if(task.getId().equals(t.getId())) break;
            pos++;
        }

        taskEndpoint.changeTaskStatusByIndex(session, pos, Status.COMPLETE);
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeTaskStatusByNameTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.changeTaskStatusByName(session, task.getName(), Status.COMPLETE);
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllByProjectIdTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        final Task task1 = taskEndpoint.addTask(session, "taskTest", "descrTest");
        final Task task2 = taskEndpoint.addTask(session, "taskTest2", "descrTest");
        final Task task3 = taskEndpoint.addTask(session, "taskTest3", "descrTest");
        taskEndpoint.bindTaskByProject(session, project.getId(), task1.getId());
        taskEndpoint.bindTaskByProject(session, project.getId(), task2.getId());
        taskEndpoint.bindTaskByProject(session, project.getId(), task3.getId());
        Assert.assertEquals(3, taskEndpoint.findAllByProjectId(session, project.getId()).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllTaskTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest2", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        Assert.assertEquals(3, taskEndpoint.findAllTask(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskAllWithComparatorTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "ctaskTest", "descrTest");
        taskEndpoint.addTask(session, "ataskTest2", "descrTest");
        taskEndpoint.addTask(session, "btaskTest3", "descrTest");
        final List<Task> tasks = taskEndpoint.findTaskAllWithComparator(session, "NAME");
        Assert.assertEquals("ataskTest2", tasks.get(0).getName());
        Assert.assertEquals("btaskTest3", tasks.get(1).getName());
        Assert.assertEquals("ctaskTest", tasks.get(2).getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByIdTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        Assert.assertNotNull(taskEndpoint.findTaskOneById(session, task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByIndexTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        final Task task = taskEndpoint.findAllTask(session).get(1);
        final Task taskFind = taskEndpoint.findTaskOneByIndex(session, 1);
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findTaskOneByNameTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        final Task taskFind = taskEndpoint.findTaskOneByName(session, "taskTest");
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishTaskByIdTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.finishTaskById(session, task.getId());
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishTaskByIndexTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        final Task task = taskEndpoint.findAllTask(session).get(1);
        taskEndpoint.finishTaskByIndex(session, 1);
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishTaskByNameTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        taskEndpoint.finishTaskByName(session, "taskTest");
        final Task taskChanged = taskEndpoint.findTaskOneByName(session, task.getName());
        Assert.assertEquals(Status.COMPLETE, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        Assert.assertNotNull(taskEndpoint.findTaskOneById(session, task.getId()));
        Assert.assertTrue(taskEndpoint.removeTask(session, task));
        Assert.assertNull(taskEndpoint.findTaskOneById(session, task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskOneByIdTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        Assert.assertNotNull(taskEndpoint.findTaskOneById(session, task.getId()));
        Assert.assertTrue(taskEndpoint.removeTaskOneById(session, task.getId()));
        Assert.assertNull(taskEndpoint.findTaskOneById(session, task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskOneByIndexTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        final Task task = taskEndpoint.findAllTask(session).get(1);
        Assert.assertNotNull(taskEndpoint.findTaskOneById(session, task.getId()));
        Assert.assertTrue(taskEndpoint.removeTaskOneByIndex(session, 1));
        Assert.assertNull(taskEndpoint.findTaskOneById(session, task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeTaskOneByNameTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        Assert.assertNotNull(taskEndpoint.findTaskOneById(session, task.getId()));
        Assert.assertTrue(taskEndpoint.removeTaskOneByName(session, "taskTest"));
        Assert.assertNull(taskEndpoint.findTaskOneById(session, task.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startTaskByIdTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.startTaskById(session, task.getId());
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.IN_PROGRESS, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startTaskByIndexTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        final Task task = taskEndpoint.findAllTask(session).get(1);
        taskEndpoint.startTaskByIndex(session, 1);
        final Task taskChanged = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(Status.IN_PROGRESS, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startTaskByNameTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        taskEndpoint.addTask(session, "taskTest0", "descrTest");
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.addTask(session, "taskTest3", "descrTest");
        taskEndpoint.startTaskByName(session, "taskTest");
        final Task taskChanged = taskEndpoint.findTaskOneByName(session, task.getName());
        Assert.assertEquals(Status.IN_PROGRESS, taskChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unbindTaskFromProjectTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        Assert.assertNull(taskEndpoint.unbindTaskFromProject(session, task.getId()).getProjectId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void updateTaskByIdTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final String newName = "taskTestNew";
        final String newDescription = "descrTestNew";
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.updateTaskById(session, task.getId(), newName, newDescription);
        final Task taskUpdate = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(newName, taskUpdate.getName());
        Assert.assertEquals(newDescription, taskUpdate.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void updateTaskByIndexTest() {
        final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();
        final String newName = "taskTestNew";
        final String newDescription = "descrTestNew";
        final Task task = taskEndpoint.addTask(session, "taskTest", "descrTest");
        taskEndpoint.updateTaskByIndex(session, 0, newName, newDescription);
        final Task taskUpdate = taskEndpoint.findTaskOneById(session, task.getId());
        Assert.assertEquals(newName, taskUpdate.getName());
        Assert.assertEquals(newDescription, taskUpdate.getDescription());
    }

}

