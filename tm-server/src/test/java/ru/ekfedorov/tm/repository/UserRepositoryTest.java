package ru.ekfedorov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.marker.DBCategory;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.service.ConnectionService;
import ru.ekfedorov.tm.service.PropertyService;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    final Connection connection = connectionService.getConnection();

    final IUserRepository userRepository = new UserRepository(connection);

    @After
    @SneakyThrows
    public void after() {
        connection.commit();
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userRepository.addAll(users);
        Assert.assertTrue(userRepository.findOneById(user1.getId()).isPresent());
        Assert.assertTrue(userRepository.findOneById(user2.getId()).isPresent());
        userRepository.remove(user1);
        userRepository.remove(user2);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void addTest() {
        final User user = new User();
        Assert.assertNotNull(userRepository.add(user));
        userRepository.remove(user);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void containsTest() {
        final User user1 = new User();
        final String userId = user1.getId();
        userRepository.add(user1);
        Assert.assertTrue(userRepository.contains(userId));
        userRepository.remove(user1);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void findAll() {
        final int userSize = userRepository.findAll().size();
        final List<User> users = new ArrayList<>();
        final User user1 = new User();
        final User user2 = new User();
        users.add(user1);
        users.add(user2);
        userRepository.addAll(users);
        Assert.assertEquals(2 + userSize, userRepository.findAll().size());
        userRepository.remove(user1);
        userRepository.remove(user2);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void findByLogin() {
        final User user = new User();
        user.setLogin("testRepFindByLogin");
        userRepository.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userRepository.findByLogin(login).isPresent());
        userRepository.remove(user);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final User user1 = new User();
        final String userId = user1.getId();
        userRepository.add(user1);
        Assert.assertNotNull(userRepository.findOneById(userId));
        userRepository.remove(user1);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final User user = new User();
        userRepository.add(user);
        final String userId = user.getId();
        Assert.assertTrue(userRepository.findOneById(userId).isPresent());
        userRepository.remove(user);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void isLoginExist() {
        final User user = new User();
        user.setLogin("testRepIsExist");
        userRepository.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        Assert.assertTrue(userRepository.isLoginExist(login));
        userRepository.remove(user);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void removeByLogin() {
        final User user = new User();
        user.setLogin("testRepRemoveByLogin");
        userRepository.add(user);
        final String login = user.getLogin();
        Assert.assertNotNull(login);
        userRepository.removeByLogin(login);
        Assert.assertFalse(userRepository.isLoginExist(login));
        userRepository.remove(user);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final User user = new User();
        userRepository.add(user);
        final String userId = user.getId();
        userRepository.removeOneById(userId);
        Assert.assertFalse(userRepository.findOneById(userId).isPresent());
        userRepository.remove(user);
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void removeTest() {
        final User user = new User();
        userRepository.add(user);
        Assert.assertNotNull(userRepository.remove(user));
        userRepository.remove(user);
    }

}
