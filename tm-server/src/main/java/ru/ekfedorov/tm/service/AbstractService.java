package ru.ekfedorov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.IBusinessRepository;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.api.service.IConnectionService;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.empty.IdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.LoginIsEmptyException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.AbstractEntity;

import java.sql.Connection;
import java.util.List;
import java.util.Optional;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public abstract IRepository<E> getRepository(@NotNull Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public E add(@Nullable final E entity) {
        if (entity == null) throw new NullObjectException();
        final Connection connection = connectionService.getConnection();
        try {
            @NotNull final IRepository<E> repository = getRepository(connection);
            repository.add(entity);
            connection.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<E> entities) {
        if (entities == null) throw new NullObjectException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            repository.addAll(entities);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAllSort(
            @Nullable final String sort
    ) {
        if (isEmpty(sort)) throw new NullComparatorException();
        final Connection connection = connectionService.getConnection();
        final IRepository<E> businessRepository = getRepository(connection);
        @Nullable Sort sortType = Sort.valueOf(sort);
        return businessRepository.findAllSort(sortType.toString());
    }

    @Nullable
    @Override
    @SneakyThrows
    public E remove(@Nullable final E entity) {
        if (entity == null) throw new NullObjectException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            E entityReturn = repository.remove(entity);
            connection.commit();
            return entityReturn;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        return repository.contains(id);
    }


    @NotNull
    @Override
    @SneakyThrows
    public Optional<E> findOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Connection connection = connectionService.getConnection();
        @NotNull final IRepository<E> repository = getRepository(connection);
        return repository.findOneById(id);
    }

    @SneakyThrows
    @Override
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        final Connection connection = connectionService.getConnection();
        try {
            final IRepository<E> repository = getRepository(connection);
            repository.removeOneById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
