package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.entity.IWBS;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractBusinessEntity implements IWBS {

    @Nullable
    private String projectId;

    public Task(@Nullable final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

}
