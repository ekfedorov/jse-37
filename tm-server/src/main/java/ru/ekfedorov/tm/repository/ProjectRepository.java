package ru.ekfedorov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.constant.ConstantField;
import ru.ekfedorov.tm.constant.TableConst;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    public ProjectRepository(Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public @NotNull Project add(@NotNull Project project) {
        @NotNull final String query =
                "INSERT INTO " + getTableName() +
                        "(`id`, `name`, `description`, `user_id`, `created`, " +
                        "`date_start`, `date_finish`, `status`) " +
                        "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getUserId());
        statement.setDate(5, prepare(project.getCreated()));
        statement.setDate(6, prepare(project.getDateStart()));
        statement.setDate(7, prepare(project.getDateFinish()));
        statement.setString(8, project.getStatus().toString());
        statement.execute();
        return project;
    }

    public String getTableName() {
        return TableConst.PROJECT;
    }

    @Nullable
    @SneakyThrows
    public Project fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull Project project = new Project();
        project.setId(row.getString(ConstantField.ID));
        project.setName(row.getString(ConstantField.NAME));
        project.setDescription(row.getString(ConstantField.DESCRIPTION));
        project.setDateStart(row.getDate(ConstantField.DATE_START));
        project.setDateFinish(row.getDate(ConstantField.DATE_FINISH));
        project.setUserId(row.getString(ConstantField.USER_ID));
        project.setStatus(Status.valueOf(row.getString(ConstantField.STATUS)));
        return project;
    }


}
