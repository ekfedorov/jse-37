package ru.ekfedorov.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Getter
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    private final Connection connection;

    protected abstract String getTableName();

    protected abstract E fetch(@Nullable final ResultSet row);

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Nullable
    public java.sql.Date prepare(@Nullable final java.util.Date date) {
        if (date == null) return null;
        return new java.sql.Date(date.getTime());
    }

    @Override
    public void addAll(@NotNull final List<E> entities) {
        entities.forEach(this::add);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "DELETE FROM " + getTableName();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAllSort(@NotNull String sort) {
        @NotNull final String query =
                "SELECT * FROM " + getTableName() +
                " ORDER BY " + getTableName() + "." + sort.toLowerCase();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull ResultSet resultSet = statement.executeQuery();
        @NotNull List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    public E remove(@NotNull final E entity) {
        removeOneById(entity.getId());
        return entity;
    }


    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final String query = "SELECT * FROM " + getTableName();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull ResultSet resultSet = statement.executeQuery();
        @NotNull List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<E> findOneById(@Nullable final String id) {
        if (isEmpty(id)) return Optional.empty();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE `id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return Optional.empty();
        @Nullable Optional<E> session = Optional.ofNullable(fetch(resultSet));
        statement.close();
        return session;
    }

    @Override
    @SneakyThrows
    public void removeOneById(final String id) {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
        statement.close();
    }

    @Override
    public boolean contains(@NotNull final String id) {
        return findOneById(id).isPresent();
    }

}
