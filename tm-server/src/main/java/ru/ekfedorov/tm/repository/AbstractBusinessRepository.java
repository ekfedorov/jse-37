package ru.ekfedorov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.IBusinessRepository;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity>
        extends AbstractRepository<E> implements IBusinessRepository<E> {

    public AbstractBusinessRepository(Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        if (isEmpty(userId)) return;
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE `user_id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.execute();
        statement.close();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) return new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE `user_id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull ResultSet resultSet = statement.executeQuery();
        @NotNull List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(
            @Nullable final String userId, @NotNull final String sort
    ) {
        if (isEmpty(userId)) return new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE `user_id` = ?" +
                " ORDER BY " + getTableName() + "." + sort.toLowerCase();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull ResultSet resultSet = statement.executeQuery();
        @NotNull List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<E> findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (isEmpty(id)) return Optional.empty();
        @NotNull final String query = "SELECT * FROM " + getTableName() +
                " WHERE `id` = ? " +
                "AND `user_id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return Optional.empty();
        @Nullable Optional<E> entity = Optional.ofNullable(fetch(resultSet));
        statement.close();
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<E> findOneByIndex(
            @Nullable final String userId, @NotNull final Integer index
    ) {
        if (isEmpty(userId)) return Optional.empty();
        @NotNull final String query =
                "SELECT * FROM " + getTableName() +
                " WHERE `user_id` = ?" +
                " LIMIT ?, 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setInt(2, index);
        @NotNull ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return Optional.empty();
        @Nullable Optional<E> entity = Optional.ofNullable(fetch(resultSet));
        statement.close();
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<E> findOneByName(
            @Nullable final String userId, @NotNull final String name
    ) {
        if (isEmpty(userId)) return Optional.empty();
        @NotNull final String query =
                "SELECT * FROM " + getTableName() +
                " WHERE `name` = ?" +
                " AND `user_id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        @NotNull ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return Optional.empty();
        @Nullable Optional<E> entity = Optional.ofNullable(fetch(resultSet));
        statement.close();
        return entity;
    }

    @Override
    @SneakyThrows
    public boolean remove(
            @Nullable final String userId, @NotNull final E entity
    ) {
        @NotNull final String query =
                "DELETE FROM " + getTableName() +
                " WHERE `id` = ?" +
                " AND `user_id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, userId);
        statement.execute();
        statement.close();
        return true;
    }

    @Override
    @SneakyThrows
    public boolean removeOneById(
            @Nullable final String userId, @NotNull final String id
    ) {
        if (isEmpty(userId)) return false;
        @NotNull final String query =
                "DELETE FROM " + getTableName() +
                " WHERE `id` = ?" +
                " AND `user_id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.execute();
        statement.close();
        return true;
    }

    @Override
    @SneakyThrows
    public boolean removeOneByIndex(
            @Nullable final String userId, @NotNull final Integer index
    ) {
        if (isEmpty(userId)) return false;
        @NotNull final Optional<E> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) return false;
        removeOneById(entity.get().getId());
        return true;
    }

    @Override
    @SneakyThrows
    public boolean removeOneByName(
            @Nullable final String userId, @NotNull final String name
    ) {
        if (isEmpty(userId)) return false;
        @NotNull final String query =
                "DELETE FROM " + getTableName() +
                " WHERE `name` = ?" +
                " AND `user_id` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, userId);
        statement.execute();
        statement.close();
        return true;
    }

    @Override
    @SneakyThrows
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable String id,
            @Nullable Status status) {
        if (isEmpty(id)) return;
        if (status == null) return;
        @NotNull final String query =
                "UPDATE " + getTableName() +
                " SET `status` = ?" +
                " WHERE `id` = ?" +
                " LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, id);
        statement.execute();
        statement.close();
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(id)) return;
        if (isEmpty(name)) return;
        if (isEmpty(description)) return;
        @NotNull final String query =
                "UPDATE " + getTableName() +
                " SET `name` = ?," +
                " `description` = ?" +
                " WHERE `id` = ?" +
                " LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, id);
        statement.execute();
        statement.close();
    }

}
