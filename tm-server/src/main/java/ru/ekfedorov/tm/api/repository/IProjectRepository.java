package ru.ekfedorov.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.IBusinessRepository;
import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IBusinessRepository<Project> {
}
